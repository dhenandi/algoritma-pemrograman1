#include<iostream.h>
#include<conio.h>

float lingkaran(float a);

main(){
    float tinggi,jari,luas;

    cout << "======================================" << endl;
    cout << "PROGRAM MENGHITUNG LUAS SELIMUT TABUNG" << endl;
    cout << "======================================" << endl;
    cout << "Masukkan Nilai Jari-Jari : "; cin>>jari;
    cout << "Masukkan Nilai Tinggi : "; cin>>tinggi;
    cout << "======================================" << endl;

    luas=(2 * lingkaran(jari)) * tinggi;
    //luas=lingkaran(jari);

    cout << "Nilai luas selimut tabung adalah : "<<luas;
    getch();
}

float lingkaran(float a){
    float hasil;
    hasil = 3.142857142857143 * a;
    return hasil;
}