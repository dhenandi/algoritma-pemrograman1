#include <stdio.h>
#include <conio.h>

main()
{
    printf("==========================\n");
    printf("Pengecekan Grade Bagian 2 \n");
    printf("==========================\n");
    printf("A. Grade A \n");               
    printf("B. Grade B \n");
    printf("C. Grade C \n");
    printf("D. Grade D \n");

    char pilihan;
    printf("Masukan Pilihan : ");
    scanf("%c",&pilihan);
    printf("==========================\n");
    switch(pilihan)
    {
     case 'A':
          printf("Nilai Grade A ialah nilai 80-100");
          break;
     case 'B':
          printf("Nilai Grade B ialah nilai 65-79");
          break;
     case 'C':
          printf("Nilai Grade C ialah nilai 50-64");
          break;
     case 'D':
          printf("Nilai Grade D ialah nilai < 49");
          break;
     default:
          printf("Tidak Ada Grade");
          break;
    }
    getch();
}