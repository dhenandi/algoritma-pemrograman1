#include <stdio.h>
#include <conio.h>

main()
{
    printf("==========================\n");
    printf("Pengecekan Grade Bagian 1 \n");
    printf("==========================\n");
    printf("1. Nilai 80-100 \n");          
    printf("2. Nilai 65-79 \n");
    printf("3. Nilai 50-64 \n");
    printf("4. Nilai < 49 \n \n");

    int pilihan;
    printf("Masukan Pilihan : ");
    scanf("%i",&pilihan);
    printf("==========================\n");
    printf("Nilai Grade Anda Adalah ");
    switch(pilihan)
    {
     case 1:
          printf("A");
          break;
     case 2:
          printf("B");
          break;
     case 3:
          printf("C");
          break;
     case 4:
          printf("D");
          break;
     default:
          printf("Tidak Ada Grade");
          break;
    }
    getch();
}