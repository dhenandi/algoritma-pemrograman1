#include<iostream.h>
#include<conio.h>

float lingkaran(float a);
float tabung(float jari, float tinggi);

main(){
    float tinggi,jari;

    cout << "======================================" << endl;
    cout << "PROGRAM MENGHITUNG LUAS SELIMUT TABUNG" << endl;
    cout << "======================================" << endl;
    cout << "Masukkan Nilai Jari-Jari : "; cin>>jari;
    cout << "Masukkan Nilai Tinggi : "; cin>>tinggi;
    cout << "======================================" << endl;
    cout << "Nilai luas selimut tabung adalah : "<<tabung(jari,tinggi);
    getch();
}

float tabung(float jari, float tinggi){
    return (2 * lingkaran(jari)) * tinggi;
}

float lingkaran(float a){
    float hasil;
    //hasil = (22/7) * a;
    hasil = 3.142857142857143 * a;
    return hasil;
}