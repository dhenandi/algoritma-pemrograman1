# Repostitory Algoritma Pemrograman Semester 1

Ini adalah repo algoritma pemrograman semester 1

# Pertemuan 20 Januari 2018

## Array Multi Dimensi

**string.h** = library untuk membuat char lebih panjang <br>
* Contoh program


```c
int main(){

char negara[4][2][15] = {{"Indonesia","Jakarta"},{"Filipina","Manila"},{"Prancis","Paris"},{"Pakistan","Islamabad"}} 

for (int baris = 0; baris < 4; baris++){            
    if (negara[baris][0][0]=='P'){                  
        cout<<negara[baris][0]<<"-"<<negara[baris][1]<<"\n";        
    getch();
    }
```
* Maksudnya untuk integer baris sampai kurang dari 4 <br>
* [0] pertama ambil array, 0 kedua adalah char(kalimat pertama = P) <br>
* Tampilkan jika P<br>

# Pertemuan ke 13 dan 14, 21 Januari 2018

## Function dan Prosedur

Prosedur kurang lebih adalah penyederhanaan program. 

Keuntungan : 

* Program lebih pendek
* Mudah didokumentasi
* Mengurangi kesalahan 
* Mudah mencari kesalahan

Fungsi adalah kumpulan intruksi perintah program yang dikelompokan menjadi satu, letaknya terpisah dari program yang menggunakan fungsi tersebut, memiliki nama tertentu yang untuk, dam digunakan untuk mengerjakan suatu tujuan tertentu 

Standar librari fungsi adalah iostream.

dalam membuat fungsi perlu diperhatikan :

* Data yang diperlukan sebagai inputan 
* Informasi apa yang harus diberikan oleh fungsi yang dibuat ke pemanggilnya
* Algoritma apa yang harus digunakan untuk mengolah data menjadi informasi

**Void** adalah tidak ada tipe, fungsi yang tidak mengembalikan nilai. Fungsi void sering disebut prosedur.
**Non Void** fungsi yang mengembalikan.

Ciri ciri void :

* Tidak ada keyword return,
* Tidak adanya tipe data di dalam deklarasi fungsi
* Menggunakan keyword *void*
* Tidak dapat langsung ditampilkan hasilnya.

Contoh program sederhana : 

```c
#include<iostream.h>
#include<conio.h>

void cetak()
{
    cout<<"Belajar C++";
    getch();
}

void main()
{
    cetak();
}
```

Contoh program hitung barang menggunakan main : 

```c
#include<conio.h>
#include<iostream.h>

void penjualan (float a, float b) {
    float total;
    total = a * b;
    cout << "Harga Barang : " << total << endl;
}


void main(){
    float harga, jumlah;
    cout << "============================" << endl;
    cout << "PROGRAM HITUNG HARGA BARANG" << endl;
    cout << "============================" << endl;
    cout << "Masukkan Harga Barang : " ; cin >> harga;
    cout << "Masukkan Jumlah Barang : "; cin >> jumlah;
    cout << "============================" << endl;
    cout << "\n";
    penjualan(harga,jumlah);
    getch();
} 
```

## Function

Fungsi non-void adalah fungsi yang mengembalikan nilai <br>

ciri-ciri :

* Ada keyword return
* Ada tipe data yang mengawali deklarasi fungsi 
* Tidak ada void
* memiliki nilai kembalian
* Dapat dianalogikan sebagai suatu variabel yang memiliki tipe data tertentu sehingga dapat langsung ditampilkan hasilnya

Fungsi dapat memanggil fungsi lain, bahkan dapat memanggil fungsi sendiri.

```c
#include<iostream.h>
#include<conio.h>

int luas(int p, int l){
    return (p*l);
}

main(){
    int pj,lb;
    cout<<"Panjang = ";cin>>pj;
    cout<<"Lebar = ";cin>>lb;
    cout<<"\nLuas = "<<luas(pj,lb);
    getch();
}
```


